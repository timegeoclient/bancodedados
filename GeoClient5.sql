toc.dat                                                                                             0000600 0004000 0002000 00000034721 12745734717 0014467 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP                           t         	   GeoClient    9.5.2    9.5.2 0    k           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false         l           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false         m           1262    16384 	   GeoClient    DATABASE     �   CREATE DATABASE "GeoClient" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Portuguese_Brazil.1252' LC_CTYPE = 'Portuguese_Brazil.1252';
    DROP DATABASE "GeoClient";
             postgres    false                     2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false         n           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    6         o           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    6                     3079    12355    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false         p           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1         �            1259    16395    administrador    TABLE       CREATE TABLE administrador (
    id_adm integer NOT NULL,
    nome_adm character varying(200) NOT NULL,
    usuario_adm character varying(20),
    senha_adm character varying(32) NOT NULL,
    id_empresa integer NOT NULL,
    email_adm character varying(150)
);
 !   DROP TABLE public.administrador;
       public         postgres    false    6         �            1259    16393    administrador_id_adm_seq    SEQUENCE     z   CREATE SEQUENCE administrador_id_adm_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.administrador_id_adm_seq;
       public       postgres    false    6    184         q           0    0    administrador_id_adm_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE administrador_id_adm_seq OWNED BY administrador.id_adm;
            public       postgres    false    183         �            1259    16408    colaborador    TABLE     =  CREATE TABLE colaborador (
    nome_colab character varying(200) NOT NULL,
    usuario_colab character varying(20) NOT NULL,
    senha_colab character varying(32) NOT NULL,
    mac_colab character varying(17) NOT NULL,
    so_colab character varying(30) NOT NULL,
    vesao_colab character varying(10) NOT NULL,
    id_colab integer NOT NULL,
    validacao_colab boolean DEFAULT false NOT NULL,
    chave_verificacao character varying(40),
    id_empresa integer NOT NULL,
    numero_colab character varying(20) NOT NULL,
    email_colab character varying(150) NOT NULL
);
    DROP TABLE public.colaborador;
       public         postgres    false    6         �            1259    16406    colaborador_id_colab_seq    SEQUENCE     z   CREATE SEQUENCE colaborador_id_colab_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.colaborador_id_colab_seq;
       public       postgres    false    6    186         r           0    0    colaborador_id_colab_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE colaborador_id_colab_seq OWNED BY colaborador.id_colab;
            public       postgres    false    185         �            1259    16387    empresa    TABLE     �   CREATE TABLE empresa (
    id_empresa integer NOT NULL,
    nome_empresa character varying(50) NOT NULL,
    cnpj_empresa character varying(18)
);
    DROP TABLE public.empresa;
       public         postgres    false    6         �            1259    16385    emprese_id_empresa_seq    SEQUENCE     x   CREATE SEQUENCE emprese_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.emprese_id_empresa_seq;
       public       postgres    false    6    182         s           0    0    emprese_id_empresa_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE emprese_id_empresa_seq OWNED BY empresa.id_empresa;
            public       postgres    false    181         �            1259    16425    localizacao    TABLE     �  CREATE TABLE localizacao (
    id_loca integer NOT NULL,
    satelites_ativos integer,
    precisao_loca character varying(20),
    nome_cliente character varying(200) NOT NULL,
    longitude_cliente character varying(20),
    latitude_cliente character varying(20),
    logradoro_cliente character varying(50),
    numero_cliente character varying(8) NOT NULL,
    complemento_cliente character varying(100),
    bairro_cliente character varying(100) NOT NULL,
    cep_cliente character varying(9) NOT NULL,
    cidade_cliente character varying(100) NOT NULL,
    estado_cliente character varying(2) NOT NULL,
    rua_cliente character varying(200) NOT NULL,
    id_empresa integer,
    id_colab integer,
    status_visita character varying(50)
);
    DROP TABLE public.localizacao;
       public         postgres    false    6         �            1259    16423    localizacao_id_loca_seq    SEQUENCE     y   CREATE SEQUENCE localizacao_id_loca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.localizacao_id_loca_seq;
       public       postgres    false    188    6         t           0    0    localizacao_id_loca_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE localizacao_id_loca_seq OWNED BY localizacao.id_loca;
            public       postgres    false    187         �            1259    16446    visitas    TABLE     �  CREATE TABLE visitas (
    id_colab integer NOT NULL,
    id_loca integer NOT NULL,
    visita_feita character varying(3) NOT NULL,
    id_visita integer NOT NULL,
    latitude_visi character varying(20),
    longitude_visi character varying(20),
    data_atribuida date,
    data_retorn date,
    data_aprov date,
    satelitesativos_vis integer,
    precisao_vis character varying(20),
    status_vis character varying(25),
    logradouro_vis character varying(40),
    numero_vis character varying(6),
    bairro_vis character varying(100),
    cidade_vis character varying(60),
    dt_rejeicao date,
    rua_vis character varying(300)
);
    DROP TABLE public.visitas;
       public         postgres    false    6         �            1259    16444    visitas_id_visita_seq    SEQUENCE     w   CREATE SEQUENCE visitas_id_visita_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.visitas_id_visita_seq;
       public       postgres    false    190    6         u           0    0    visitas_id_visita_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE visitas_id_visita_seq OWNED BY visitas.id_visita;
            public       postgres    false    189         �           2604    16398    id_adm    DEFAULT     n   ALTER TABLE ONLY administrador ALTER COLUMN id_adm SET DEFAULT nextval('administrador_id_adm_seq'::regclass);
 C   ALTER TABLE public.administrador ALTER COLUMN id_adm DROP DEFAULT;
       public       postgres    false    184    183    184         �           2604    16411    id_colab    DEFAULT     n   ALTER TABLE ONLY colaborador ALTER COLUMN id_colab SET DEFAULT nextval('colaborador_id_colab_seq'::regclass);
 C   ALTER TABLE public.colaborador ALTER COLUMN id_colab DROP DEFAULT;
       public       postgres    false    186    185    186         �           2604    16390 
   id_empresa    DEFAULT     j   ALTER TABLE ONLY empresa ALTER COLUMN id_empresa SET DEFAULT nextval('emprese_id_empresa_seq'::regclass);
 A   ALTER TABLE public.empresa ALTER COLUMN id_empresa DROP DEFAULT;
       public       postgres    false    182    181    182         �           2604    16428    id_loca    DEFAULT     l   ALTER TABLE ONLY localizacao ALTER COLUMN id_loca SET DEFAULT nextval('localizacao_id_loca_seq'::regclass);
 B   ALTER TABLE public.localizacao ALTER COLUMN id_loca DROP DEFAULT;
       public       postgres    false    187    188    188         �           2604    16449 	   id_visita    DEFAULT     h   ALTER TABLE ONLY visitas ALTER COLUMN id_visita SET DEFAULT nextval('visitas_id_visita_seq'::regclass);
 @   ALTER TABLE public.visitas ALTER COLUMN id_visita DROP DEFAULT;
       public       postgres    false    190    189    190         b          0    16395    administrador 
   TABLE DATA               a   COPY administrador (id_adm, nome_adm, usuario_adm, senha_adm, id_empresa, email_adm) FROM stdin;
    public       postgres    false    184       2146.dat v           0    0    administrador_id_adm_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('administrador_id_adm_seq', 1, false);
            public       postgres    false    183         d          0    16408    colaborador 
   TABLE DATA               �   COPY colaborador (nome_colab, usuario_colab, senha_colab, mac_colab, so_colab, vesao_colab, id_colab, validacao_colab, chave_verificacao, id_empresa, numero_colab, email_colab) FROM stdin;
    public       postgres    false    186       2148.dat w           0    0    colaborador_id_colab_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('colaborador_id_colab_seq', 1, false);
            public       postgres    false    185         `          0    16387    empresa 
   TABLE DATA               B   COPY empresa (id_empresa, nome_empresa, cnpj_empresa) FROM stdin;
    public       postgres    false    182       2144.dat x           0    0    emprese_id_empresa_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('emprese_id_empresa_seq', 1, false);
            public       postgres    false    181         f          0    16425    localizacao 
   TABLE DATA               "  COPY localizacao (id_loca, satelites_ativos, precisao_loca, nome_cliente, longitude_cliente, latitude_cliente, logradoro_cliente, numero_cliente, complemento_cliente, bairro_cliente, cep_cliente, cidade_cliente, estado_cliente, rua_cliente, id_empresa, id_colab, status_visita) FROM stdin;
    public       postgres    false    188       2150.dat y           0    0    localizacao_id_loca_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('localizacao_id_loca_seq', 13, true);
            public       postgres    false    187         h          0    16446    visitas 
   TABLE DATA                 COPY visitas (id_colab, id_loca, visita_feita, id_visita, latitude_visi, longitude_visi, data_atribuida, data_retorn, data_aprov, satelitesativos_vis, precisao_vis, status_vis, logradouro_vis, numero_vis, bairro_vis, cidade_vis, dt_rejeicao, rua_vis) FROM stdin;
    public       postgres    false    190       2152.dat z           0    0    visitas_id_visita_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('visitas_id_visita_seq', 1, false);
            public       postgres    false    189         �           2606    16400    id_adm 
   CONSTRAINT     O   ALTER TABLE ONLY administrador
    ADD CONSTRAINT id_adm PRIMARY KEY (id_adm);
 >   ALTER TABLE ONLY public.administrador DROP CONSTRAINT id_adm;
       public         postgres    false    184    184         �           2606    16417    id_colab 
   CONSTRAINT     Q   ALTER TABLE ONLY colaborador
    ADD CONSTRAINT id_colab PRIMARY KEY (id_colab);
 >   ALTER TABLE ONLY public.colaborador DROP CONSTRAINT id_colab;
       public         postgres    false    186    186         �           2606    16392 
   id_empresa 
   CONSTRAINT     Q   ALTER TABLE ONLY empresa
    ADD CONSTRAINT id_empresa PRIMARY KEY (id_empresa);
 <   ALTER TABLE ONLY public.empresa DROP CONSTRAINT id_empresa;
       public         postgres    false    182    182         �           2606    16433    id_loca 
   CONSTRAINT     O   ALTER TABLE ONLY localizacao
    ADD CONSTRAINT id_loca PRIMARY KEY (id_loca);
 =   ALTER TABLE ONLY public.localizacao DROP CONSTRAINT id_loca;
       public         postgres    false    188    188         �           2606    16451 	   id_visita 
   CONSTRAINT     O   ALTER TABLE ONLY visitas
    ADD CONSTRAINT id_visita PRIMARY KEY (id_visita);
 ;   ALTER TABLE ONLY public.visitas DROP CONSTRAINT id_visita;
       public         postgres    false    190    190         �           2606    16452    id_colab    FK CONSTRAINT     n   ALTER TABLE ONLY visitas
    ADD CONSTRAINT id_colab FOREIGN KEY (id_colab) REFERENCES colaborador(id_colab);
 :   ALTER TABLE ONLY public.visitas DROP CONSTRAINT id_colab;
       public       postgres    false    186    2019    190         �           2606    16401 
   id_empresa    FK CONSTRAINT     v   ALTER TABLE ONLY administrador
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES empresa(id_empresa);
 B   ALTER TABLE ONLY public.administrador DROP CONSTRAINT id_empresa;
       public       postgres    false    182    184    2015         �           2606    16418 
   id_empresa    FK CONSTRAINT     t   ALTER TABLE ONLY colaborador
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES empresa(id_empresa);
 @   ALTER TABLE ONLY public.colaborador DROP CONSTRAINT id_empresa;
       public       postgres    false    2015    182    186         �           2606    16434 
   id_empresa    FK CONSTRAINT     t   ALTER TABLE ONLY localizacao
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES empresa(id_empresa);
 @   ALTER TABLE ONLY public.localizacao DROP CONSTRAINT id_empresa;
       public       postgres    false    182    2015    188         �           2606    16457    id_loca    FK CONSTRAINT     k   ALTER TABLE ONLY visitas
    ADD CONSTRAINT id_loca FOREIGN KEY (id_loca) REFERENCES localizacao(id_loca);
 9   ALTER TABLE ONLY public.visitas DROP CONSTRAINT id_loca;
       public       postgres    false    2021    190    188                                                       2146.dat                                                                                            0000600 0004000 0002000 00000000005 12745734717 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2148.dat                                                                                            0000600 0004000 0002000 00000000005 12745734717 0014264 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2144.dat                                                                                            0000600 0004000 0002000 00000000005 12745734717 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2150.dat                                                                                            0000600 0004000 0002000 00000000455 12745734717 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        6	\N	\N	Hismael	\N	\N	\N	4567	\N	Centro	62900-000	Russas	Ce	Coronel a	\N	\N	\N
7	\N	\N	Hismael	\N	\N	\N	4567	\N	Centro	62900-000	Russas	Ce	Coronel a	\N	\N	\N
8	\N	\N	Fran	\N	\N	\N	453	\N	baixo	63400-000	Cedro	Ce	Cntodoo	\N	\N	\N
9	\N	\N	Neto	\N	\N	\N	3243	\N	vedr	43568-000	sp	sp	bdggeb	\N	\N	\N
\.


                                                                                                                                                                                                                   2152.dat                                                                                            0000600 0004000 0002000 00000000005 12745734717 0014257 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           restore.sql                                                                                         0000600 0004000 0002000 00000032623 12745734717 0015413 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.visitas DROP CONSTRAINT id_loca;
ALTER TABLE ONLY public.localizacao DROP CONSTRAINT id_empresa;
ALTER TABLE ONLY public.colaborador DROP CONSTRAINT id_empresa;
ALTER TABLE ONLY public.administrador DROP CONSTRAINT id_empresa;
ALTER TABLE ONLY public.visitas DROP CONSTRAINT id_colab;
ALTER TABLE ONLY public.visitas DROP CONSTRAINT id_visita;
ALTER TABLE ONLY public.localizacao DROP CONSTRAINT id_loca;
ALTER TABLE ONLY public.empresa DROP CONSTRAINT id_empresa;
ALTER TABLE ONLY public.colaborador DROP CONSTRAINT id_colab;
ALTER TABLE ONLY public.administrador DROP CONSTRAINT id_adm;
ALTER TABLE public.visitas ALTER COLUMN id_visita DROP DEFAULT;
ALTER TABLE public.localizacao ALTER COLUMN id_loca DROP DEFAULT;
ALTER TABLE public.empresa ALTER COLUMN id_empresa DROP DEFAULT;
ALTER TABLE public.colaborador ALTER COLUMN id_colab DROP DEFAULT;
ALTER TABLE public.administrador ALTER COLUMN id_adm DROP DEFAULT;
DROP SEQUENCE public.visitas_id_visita_seq;
DROP TABLE public.visitas;
DROP SEQUENCE public.localizacao_id_loca_seq;
DROP TABLE public.localizacao;
DROP SEQUENCE public.emprese_id_empresa_seq;
DROP TABLE public.empresa;
DROP SEQUENCE public.colaborador_id_colab_seq;
DROP TABLE public.colaborador;
DROP SEQUENCE public.administrador_id_adm_seq;
DROP TABLE public.administrador;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: administrador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE administrador (
    id_adm integer NOT NULL,
    nome_adm character varying(200) NOT NULL,
    usuario_adm character varying(20),
    senha_adm character varying(32) NOT NULL,
    id_empresa integer NOT NULL,
    email_adm character varying(150)
);


ALTER TABLE administrador OWNER TO postgres;

--
-- Name: administrador_id_adm_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE administrador_id_adm_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE administrador_id_adm_seq OWNER TO postgres;

--
-- Name: administrador_id_adm_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE administrador_id_adm_seq OWNED BY administrador.id_adm;


--
-- Name: colaborador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE colaborador (
    nome_colab character varying(200) NOT NULL,
    usuario_colab character varying(20) NOT NULL,
    senha_colab character varying(32) NOT NULL,
    mac_colab character varying(17) NOT NULL,
    so_colab character varying(30) NOT NULL,
    vesao_colab character varying(10) NOT NULL,
    id_colab integer NOT NULL,
    validacao_colab boolean DEFAULT false NOT NULL,
    chave_verificacao character varying(40),
    id_empresa integer NOT NULL,
    numero_colab character varying(20) NOT NULL,
    email_colab character varying(150) NOT NULL
);


ALTER TABLE colaborador OWNER TO postgres;

--
-- Name: colaborador_id_colab_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE colaborador_id_colab_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE colaborador_id_colab_seq OWNER TO postgres;

--
-- Name: colaborador_id_colab_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE colaborador_id_colab_seq OWNED BY colaborador.id_colab;


--
-- Name: empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE empresa (
    id_empresa integer NOT NULL,
    nome_empresa character varying(50) NOT NULL,
    cnpj_empresa character varying(18)
);


ALTER TABLE empresa OWNER TO postgres;

--
-- Name: emprese_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE emprese_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE emprese_id_empresa_seq OWNER TO postgres;

--
-- Name: emprese_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE emprese_id_empresa_seq OWNED BY empresa.id_empresa;


--
-- Name: localizacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE localizacao (
    id_loca integer NOT NULL,
    satelites_ativos integer,
    precisao_loca character varying(20),
    nome_cliente character varying(200) NOT NULL,
    longitude_cliente character varying(20),
    latitude_cliente character varying(20),
    logradoro_cliente character varying(50),
    numero_cliente character varying(8) NOT NULL,
    complemento_cliente character varying(100),
    bairro_cliente character varying(100) NOT NULL,
    cep_cliente character varying(9) NOT NULL,
    cidade_cliente character varying(100) NOT NULL,
    estado_cliente character varying(2) NOT NULL,
    rua_cliente character varying(200) NOT NULL,
    id_empresa integer,
    id_colab integer,
    status_visita character varying(50)
);


ALTER TABLE localizacao OWNER TO postgres;

--
-- Name: localizacao_id_loca_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE localizacao_id_loca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE localizacao_id_loca_seq OWNER TO postgres;

--
-- Name: localizacao_id_loca_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE localizacao_id_loca_seq OWNED BY localizacao.id_loca;


--
-- Name: visitas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE visitas (
    id_colab integer NOT NULL,
    id_loca integer NOT NULL,
    visita_feita character varying(3) NOT NULL,
    id_visita integer NOT NULL,
    latitude_visi character varying(20),
    longitude_visi character varying(20),
    data_atribuida date,
    data_retorn date,
    data_aprov date,
    satelitesativos_vis integer,
    precisao_vis character varying(20),
    status_vis character varying(25),
    logradouro_vis character varying(40),
    numero_vis character varying(6),
    bairro_vis character varying(100),
    cidade_vis character varying(60),
    dt_rejeicao date,
    rua_vis character varying(300)
);


ALTER TABLE visitas OWNER TO postgres;

--
-- Name: visitas_id_visita_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE visitas_id_visita_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE visitas_id_visita_seq OWNER TO postgres;

--
-- Name: visitas_id_visita_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE visitas_id_visita_seq OWNED BY visitas.id_visita;


--
-- Name: id_adm; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador ALTER COLUMN id_adm SET DEFAULT nextval('administrador_id_adm_seq'::regclass);


--
-- Name: id_colab; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colaborador ALTER COLUMN id_colab SET DEFAULT nextval('colaborador_id_colab_seq'::regclass);


--
-- Name: id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empresa ALTER COLUMN id_empresa SET DEFAULT nextval('emprese_id_empresa_seq'::regclass);


--
-- Name: id_loca; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY localizacao ALTER COLUMN id_loca SET DEFAULT nextval('localizacao_id_loca_seq'::regclass);


--
-- Name: id_visita; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visitas ALTER COLUMN id_visita SET DEFAULT nextval('visitas_id_visita_seq'::regclass);


--
-- Data for Name: administrador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY administrador (id_adm, nome_adm, usuario_adm, senha_adm, id_empresa, email_adm) FROM stdin;
\.
COPY administrador (id_adm, nome_adm, usuario_adm, senha_adm, id_empresa, email_adm) FROM '$$PATH$$/2146.dat';

--
-- Name: administrador_id_adm_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('administrador_id_adm_seq', 1, false);


--
-- Data for Name: colaborador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY colaborador (nome_colab, usuario_colab, senha_colab, mac_colab, so_colab, vesao_colab, id_colab, validacao_colab, chave_verificacao, id_empresa, numero_colab, email_colab) FROM stdin;
\.
COPY colaborador (nome_colab, usuario_colab, senha_colab, mac_colab, so_colab, vesao_colab, id_colab, validacao_colab, chave_verificacao, id_empresa, numero_colab, email_colab) FROM '$$PATH$$/2148.dat';

--
-- Name: colaborador_id_colab_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('colaborador_id_colab_seq', 1, false);


--
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY empresa (id_empresa, nome_empresa, cnpj_empresa) FROM stdin;
\.
COPY empresa (id_empresa, nome_empresa, cnpj_empresa) FROM '$$PATH$$/2144.dat';

--
-- Name: emprese_id_empresa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('emprese_id_empresa_seq', 1, false);


--
-- Data for Name: localizacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY localizacao (id_loca, satelites_ativos, precisao_loca, nome_cliente, longitude_cliente, latitude_cliente, logradoro_cliente, numero_cliente, complemento_cliente, bairro_cliente, cep_cliente, cidade_cliente, estado_cliente, rua_cliente, id_empresa, id_colab, status_visita) FROM stdin;
\.
COPY localizacao (id_loca, satelites_ativos, precisao_loca, nome_cliente, longitude_cliente, latitude_cliente, logradoro_cliente, numero_cliente, complemento_cliente, bairro_cliente, cep_cliente, cidade_cliente, estado_cliente, rua_cliente, id_empresa, id_colab, status_visita) FROM '$$PATH$$/2150.dat';

--
-- Name: localizacao_id_loca_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('localizacao_id_loca_seq', 13, true);


--
-- Data for Name: visitas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY visitas (id_colab, id_loca, visita_feita, id_visita, latitude_visi, longitude_visi, data_atribuida, data_retorn, data_aprov, satelitesativos_vis, precisao_vis, status_vis, logradouro_vis, numero_vis, bairro_vis, cidade_vis, dt_rejeicao, rua_vis) FROM stdin;
\.
COPY visitas (id_colab, id_loca, visita_feita, id_visita, latitude_visi, longitude_visi, data_atribuida, data_retorn, data_aprov, satelitesativos_vis, precisao_vis, status_vis, logradouro_vis, numero_vis, bairro_vis, cidade_vis, dt_rejeicao, rua_vis) FROM '$$PATH$$/2152.dat';

--
-- Name: visitas_id_visita_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('visitas_id_visita_seq', 1, false);


--
-- Name: id_adm; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador
    ADD CONSTRAINT id_adm PRIMARY KEY (id_adm);


--
-- Name: id_colab; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colaborador
    ADD CONSTRAINT id_colab PRIMARY KEY (id_colab);


--
-- Name: id_empresa; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empresa
    ADD CONSTRAINT id_empresa PRIMARY KEY (id_empresa);


--
-- Name: id_loca; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY localizacao
    ADD CONSTRAINT id_loca PRIMARY KEY (id_loca);


--
-- Name: id_visita; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visitas
    ADD CONSTRAINT id_visita PRIMARY KEY (id_visita);


--
-- Name: id_colab; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visitas
    ADD CONSTRAINT id_colab FOREIGN KEY (id_colab) REFERENCES colaborador(id_colab);


--
-- Name: id_empresa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES empresa(id_empresa);


--
-- Name: id_empresa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY colaborador
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES empresa(id_empresa);


--
-- Name: id_empresa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY localizacao
    ADD CONSTRAINT id_empresa FOREIGN KEY (id_empresa) REFERENCES empresa(id_empresa);


--
-- Name: id_loca; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY visitas
    ADD CONSTRAINT id_loca FOREIGN KEY (id_loca) REFERENCES localizacao(id_loca);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             